#include "omp.h"
#include "stdio.h"
#include "stdlib.h"
#include "time.h"
#include  <sys/time.h>

#define SIZE 1048576
#define RUN_NUM 5
int main() 
{
    printf("Student1 No. : 810196684, Student2 No. : 810196683\n");

    srand(time(0));
    double start, end;
    float speedUp;
	float time1, time2;

    float *arr;
	arr = new float [SIZE];

	if (!arr) {
		printf ("Memory allocation error!!\n");
		return 1;
	}

    float avgParallelTime = 0, avgSerialTime = 0, avgSpeedup = 0;
    for (int j = 0; j < RUN_NUM; j++)
    {

    
	    // Initialize vectors with random numbers
        for (long i = 0; i < SIZE; i++)
            arr[i] = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/100.0));

        //serial
        
        int serialIndex = -1;
        float serialMaxArr = -1;
        start = omp_get_wtime();
        for (int i = 0; i < SIZE; i++)
        {
            if (arr[i] > serialMaxArr)
            {
                serialMaxArr = arr[i];
                serialIndex = i;
            }
        }
        end = omp_get_wtime();
        time1 = end - start;



        //parallel
        int i;
        int parallelIndex = -1;
        float parallelMaxArr = -1;
        float localMax;
        int localIndex;
        start = omp_get_wtime();
        #pragma omp parallel shared(arr, parallelIndex, parallelMaxArr) private(localIndex, localMax) num_threads(4)
        {
            
            localMax = -1;
            localIndex = -1;
            #pragma omp for
                for (i = 0; i < SIZE; i++)
                    if (localMax < arr[i])
                    {
                        localMax = arr[i];
                        localIndex = i;
                    }

            #pragma omp critical
            {
                if (parallelMaxArr < localMax)
                {
                    parallelMaxArr = localMax;
                    parallelIndex = localIndex;
                }
            }
        } 
        end = omp_get_wtime();
        time2 = end - start;

        printf("Run Num is %d\n", j);
        printf("Serial result arr[%d] = %f\n", serialIndex, serialMaxArr);
        printf("Parallel result arr[%d] = %f\n", parallelIndex, parallelMaxArr);

        printf("Serial run time: %f seconds\n", time1);
        printf("Parallel run time: %f seconds\n", time2);
        speedUp = (float) (time1)/(float) time2;
        printf("Speedup = %f\n\n", speedUp);

        avgParallelTime += time2;
        avgSerialTime += time1;
        avgSpeedup += speedUp;
    }

    printf("Final Result:\n");
    printf("Serial run time (avg): %f seconds\n", avgSerialTime/RUN_NUM);
    printf("Parallel run time (avg): %f seconds\n", avgParallelTime/RUN_NUM);
    printf("Speedup (avg) = %f\n\n", avgSpeedup/RUN_NUM);

}