#include "shared.h"

float parallel(float arr[])
{
    double start, stop;
    float tmp[N];

    start = omp_get_wtime();
    #pragma omp parallel num_threads(NUM_THREADS) shared(arr)
    {
        // 4 threads - 4 sections
        #pragma omp sections 
        {
            #pragma omp section 
            quickSort(arr, 0, N/4 - 1);

            #pragma omp section 
            quickSort(arr, N/4, N/2 - 1);

            #pragma omp section 
            quickSort(arr, N/2, 3*N/4 - 1);

            #pragma omp section 
            quickSort(arr, 3*N/4, N-1);
        }
        

        #pragma omp sections 
        {
            #pragma omp section 
            merge(arr, tmp, 0, N/2);

            #pragma omp section 
            merge(arr, tmp, N/2, N);

        }

        #pragma omp single
        merge(arr, tmp, 0, N);
        
    }
    stop = omp_get_wtime();

    return stop - start;
}
